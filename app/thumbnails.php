<?php 
	$stars = '
	<div class="stars">
	<i class="fas fa-star"></i>
	<i class="fas fa-star"></i>
	<i class="fas fa-star"></i>
	<i class="fas fa-star"></i>
	<i class="far fa-star"></i>
	</div>
	';
?>

<div class="productthumbnails">
	<div class="row">
		<div class="col">
			<div class="label_product">HOT</div>
			<?=$stars?>
			<img src="./images/shop/th01.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
		<div class="col">
			<div class="label_product">SALE</div>
			<?=$stars?>
			<img src="./images/shop/th02.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
		<div class="col">
			<div class="label_product">HI</div>
			<?=$stars?>
			<img src="./images/shop/th03.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
		<div class="col">
			<div class="label_product">HOT</div>
			<?=$stars?>
			<img src="./images/shop/th04.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="label_product">HOT</div>
			<?=$stars?>
			<img src="./images/shop/th05.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
		<div class="col">
			<div class="label_product">COOL</div>
			<?=$stars?>
			<img src="./images/shop/th06.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
		<div class="col">
			<div class="label_product">SALE</div>
			<?=$stars?>
			<img src="./images/shop/th07.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
		<div class="col">
			<div class="label_product">COOL</div>
			<?=$stars?>
			<img src="./images/shop/th08.jpg">
			<button class="or">BUY NOW</button>
			<button class="gr">RENT NOW</button>
		</div>
	</div>

</div>