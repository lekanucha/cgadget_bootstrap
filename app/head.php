
    <nav class="navbar navbar-expand-md navbar-light fixed-top">
      <div class="container">
      <a class="navbar-brand" href="#"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">HOME <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">GADGETS</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">AAA</a>
              <a class="dropdown-item" href="#">BBB</a>
              <a class="dropdown-item" href="#">CCC</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="product.php">SHOP</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">FAQ/ติดต่อสอบถาม</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
          <i class="fas fa-lg fa-search"></i>
          <!-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
        </form>
        <div class="navbar-nav nav_right mr-0">
          <span class="nav-item icon_user"><a href="user.php"><i class="fas fa-lg fa-user"></i> <span>Thomas Shelby</span></a></span>
          <div class="icon_cart nav-item"><span class="label_cart">20</span><a href=""><img src="images/shop.png" alt=""></a></div>
        </div>


      </div>
    <!-- </div> -->
    </nav>
