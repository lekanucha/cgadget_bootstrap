<!doctype html>
<html lang="en">
<head>
  <title>Hello, world!</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./css/app.css">
</head>
<body>
  <?php include "head.php";?>
  <main role="main">
    <div class="container">
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li><a href="#">Home</a><i></i></li>
          <li><a href="#">User</a><i></i></li>
          <li class="active" aria-current="page">Login</li>
        </ol>
      </nav>

      <div class="row arrow">
        <div class="col hid_md_down"></div>
        <div class="col center">
          <div class="overlay_top"></div>
          
          <div class="content">
            <form>
              <h1>Sign in</h1>
              <div class="form-group">
                <!-- <label for="exampleInputEmail1">Email address</label> -->
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
              <div class="form-group">
                <!-- <label for="exampleInputPassword1">Password</label> -->
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>
              
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" value="">Remember me</label>
                </div>
            
                <button type="submit" class="btn orup btn-block">Continue</button>
            
                <div class="overlay_trans"></div>
                <button type="submit" class="btn btn-gray btn-block">Create account</button>
              </form>
          </div>

          </div>
          <div class="col hid_md_down"></div>
        </div>

      <div class="row arrow profile">
        <div class="col hid_md_down"></div>
        <div class="col center">
          <div class="overlay_top"></div>

          <div class="content">
            <form>
              <h1>Register</h1>
              <div class="form-group">
                <!-- <label for="exampleInputEmail1">Email address</label> -->
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
              <div class="form-group">
                <!-- <label for="exampleInputPassword1">Password</label> -->
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>
              <div class="form-group">
                <!-- <label for="exampleInputPassword1">Password</label> -->
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Repeat password">
              </div>
            
            
                <div class="overlay_trans"></div>
            
                <button type="submit" class="btn orup btn-block">Create account</button>
            
              </form>
          </div>


          </div>
          <div class="col hid_md_down"></div>
        </div>

        <div class="row arrow">
          <div class="col hid_md_down"></div>
          <div class="col center">
            <div class="overlay_top"></div>

            <div class="content">
              <form>
                <h1>Profile</h1>
                <img class="my-3" src="./images/people/01.jpg">
                <h3 class="my-3">Thomas Shelby</h3>
                <table class="table table-sm">
                  <tbody>
                    <tr>
                      <th>Username</th>
                      <td>Ghost289</td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td>email@example.com</td>
                    </tr>
                    <tr>
                      <th>Name</th>
                      <td>George Raymond Richard Martin</td>
                    </tr>
                    <tr>
                      <th>Gender</th>
                      <td>Male</td>
                    </tr>
                    <tr>
                      <th>Age</th>
                      <td>33</td>
                    </tr>
                  </tbody></table>
                
                <div class="overlay_trans"></div>
              
                <button type="submit" class="btn btn-gray btn-block">Modify account</button>
              
              </form>
            </div>


          </div>
          <div class="col hid_md_down"></div>
        </div>


        <div class="row arrow">
          <div class="col hid_md_down"></div>
          <div class="col center">
            <div class="overlay_top"></div>

            <div class="content">
              <form>
                <h1>Modify</h1>
                <img class="my-3" src="./images/people/01.jpg">

                <div class="form-group">
                  <input type="file" class="form-control-file" id="exampleInputFile">
                </div>
                <h3 class="my-3">Thomas Shelby</h3>

        <!-- <div class="form-horizontal"> -->
        
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Username</label>
                    <div class="col-sm-8">
                    <label>Ghost289</label>
                    </div>
                  </div>
        
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Email</label>
                    <div class="col-sm-8">
                    <label>email@example.com</label>
                    </div>
                  </div>
        
        
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Name</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Name" required>
                    </div>
                  </div>
        
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Password</label>
                    <div class="col-sm-8">
                    <input type="password" class="form-control" placeholder="Password" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Repeat</label>
                    <div class="col-sm-8">
                    <input type="password" class="form-control" placeholder="Repeat password" required>
                    </div>
                  </div>
        <!-- </div> -->

                
                <div class="overlay_trans"></div>
              
                <button type="submit" class="btn orup btn-block">Modify account</button>
              
              </form>
            </div>


          </div>
          <div class="col hid_md_down"></div>
        </div>





<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a4376edef3289a6"></script>


    </div> <!-- /container -->
  </main>

<?php include "footer.php";?>



<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a4376edef3289a6"></script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


<script>
  jQuery(document).ready(function(){
    $('#spinner .input-group-addon').on('click', function(){
        let input = $(this).closest('#spinner').find('input[name=qty]');

        if($(this).data('action') === 'increment') {
            if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                input.val(parseInt(input.val(), 10) + 1);
            }
        } else if($(this).data('action') === 'decrement') {
            if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                input.val(parseInt(input.val(), 10) - 1);
            }
        }
    });
});

  jQuery(document).ready(function(){
    $('#spinner2 .input-group-addon').on('click', function(){
        let input = $(this).closest('#spinner2').find('input[name=qty]');

        if($(this).data('action') === 'increment') {
            if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                input.val(parseInt(input.val(), 10) + 1);
            }
        } else if($(this).data('action') === 'decrement') {
            if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                input.val(parseInt(input.val(), 10) - 1);
            }
        }
    });
});

</script>

</body>
</html>