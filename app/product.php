<!doctype html>
<html lang="en">
<head>
  <title>Hello, world!</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./css/app.css">
</head>
<body>
  <?php include "head.php";?>
  <main role="main">
    <div class="container">
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li><a href="#">Home</a><i></i></li>
          <li><a href="#">Top Rated</a><i></i></li>
          <li class="active" aria-current="page">DJI Phantom</li>
        </ol>
      </nav>
    
    <div class="row product">
      <div class="col">

        <div class="stars">
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-sr"></i>
          <i class="fas fa-star"></i>
          <i class="far fa-star"></i>
        </div>
        <div>
            <img src="./images/shop/th17.jpg">
        </div>
        <div class="thumbnails">
          <img src="./images/shop/th01.jpg">
          <img src="./images/shop/th02.jpg">
          <img src="./images/shop/th03.jpg">
          <img src="./images/shop/th04.jpg">
          <img src="./images/shop/th05.jpg">
          <img src="./images/shop/th06.jpg">
          <img src="./images/shop/th07.jpg">
        </div>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>

      </div>
      <div class="col grad_right">
        <div class="product_ui">
          <h1>DJI Phantom</h1>
          <div class="row">
            <div class="col-sm-6">
              <button>BUY NOW<img src="./images/icon_check.png"></button>
              <button class="grborder">RENT NOW<img src="./images/icon_check.png"></button>
            </div>
            <div class="col-sm-5 price">
              <h1>24,000</h1>
              <span>Baht</span>
            </div>
            <div class="col-sm-12">
              <form class="form-inline">
              <table>
                <tr>
                  <td>มัดจำ</td>
                  <td colspan="2">30,000 baht</td>
                </tr>
                <tr>
                  <td>จำนวน</td>
                  <td colspan="2">
                    <div class="input-group" id="spinner">
                      <div class="input-group-addon form-control-sm" data-action="decrement">
                        <i class="fas fa-sm fa-chevron-left"></i>
                      </div>
                      <input name="qty" type="text" class="form-control form-control-sm" id="number" value="1" min="1">
                      <div class="input-group-addon form-control-sm" data-action="increment">
                        <i class="fas fa-sm fa-chevron-right"></i>
                      </div>
                    </div>

                  </td>
                </tr>
                <tr>
                  <td>ระยะเวลา</td>
                  <td>
                    
                    <div class="input-group" id="spinner2">
                      <div class="input-group-addon form-control-sm" data-action="decrement">
                        <i class="fas fa-sm fa-chevron-left"></i>
                      </div>
                      <input name="qty" type="text" class="form-control form-control-sm" id="number" value="1" min="1">
                      <div class="input-group-addon form-control-sm" data-action="increment">
                        <i class="fas fa-sm fa-chevron-right"></i>
                      </div>
                    </div>
                  </td>
                  <td>
                      วัน
                  </td>
                </tr>
                <tr>
                  <td>ราตคารวม</td>
                  <td colspan="2" class="total">32,000 baht</td>
                </tr>
              </table>
            </form>
            </div>
            <div class="col-sm-8">
              <button class="addcart">ADD TO CART<img src="./images/icon_cart.png"></button>
            </div>
            <div class="col-sm-12">
              <!-- Go to www.addthis.com/dashboard to customize your tools --> 
              <div class="addthis_inline_share_toolbox"></div>
            </div>
          </div>

        </div>
        <div class="overlay_right"></div>
      </div>

    </div>

<!-- <div class="clearfix">...</div> -->

<div class="header"><h1>Video</h1></div>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/0WyqHfJOj-I" allowfullscreen></iframe>
</div>

<div class="row comments">
<div class="header"><h1>Comments</h1></div>

  <div class="row">
    <div class="col img"><img src="./images/people/01.jpg"></div>
    <div class="col">
      <div class="stars">
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="far fa-star"></i>
        </div>
      <p class="lead">Vivamus sagittis lacus</p>
      <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.</p>
      <small>Fusce dapibus</small>
    </div>
  </div>
  
  <div class="row">
  <div class="col img"><img src="./images/people/02.jpg"></div>
  <div class="col">
    <div class="stars">
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="far fa-star"></i>
      </div>
    <p class="lead">Vivamus sagittis lacus</p>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <small>Fusce dapibus</small>
  </div>
</div>

  <div class="row">
  <div class="col img"><img src="./images/people/03.jpg"></div>
  <div class="col">
    <div class="stars">
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="far fa-star"></i>
      </div>
    <p class="lead">Vivamus sagittis lacus</p>
    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.</p>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <small>Fusce dapibus</small>
  </div>
  </div>


</div>

<div class="row recommend">
  <div class="col-sm-12">

    <div class="btn-group" role="group">
      <a class="carousel-control-prev" href="#carouselC" role="button" data-slide="prev">
        <i class="fas fa-chevron-left"></i>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselC" role="button" data-slide="next">
        <i class="fas fa-chevron-right"></i>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <h3>Recommended</h3>
  </div>


  <div id="carouselC" class="carousel slide" data-ride="false">
  <div class="carousel-inner">

    <div class="carousel-item active">
      <div class="card-deck">
        <!-- large shows 4, small shows 1 -->
        <div class="card">
          <img class="card-img-top" src="./images/menu1.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Donec id elit non mi porta gravida at eget metus.</p>
            <p class="card-text"><small>12,900 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
        <div class="card hid_md_down">
          <img class="card-img-top" src="./images/menu2.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Fusce dapibus, tellus ac cursus commodo.</p>
            <p class="card-text"><small>23,500 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
        <div class="card hid_md_down">
          <img class="card-img-top" src="./images/menu3.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Etiam porta sem malesuada magna mollis euismod.</p>
            <p class="card-text"><small>23,500 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
        <div class="card hid_md_down">
          <img class="card-img-top" src="./images/menu4.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Etiam porta sem malesuada magna mollis euismod.</p>
            <p class="card-text"><small>23,500 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
      </div>
    </div>

    <div class="carousel-item">
      <div class="card-deck">
        <!-- large shows 4, small shows 1 -->
        <div class="card">
          <img class="card-img-top" src="./images/menu5.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Donec id elit non mi porta gravida at eget metus.</p>
            <p class="card-text"><small>12,900 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
        <div class="card hid_md_down">
          <img class="card-img-top" src="./images/menu2.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Fusce dapibus, tellus ac cursus commodo.</p>
            <p class="card-text"><small>23,500 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
        <div class="card hid_md_down">
          <img class="card-img-top" src="./images/menu1.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Etiam porta sem malesuada magna mollis euismod.</p>
            <p class="card-text"><small>23,500 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
        <div class="card hid_md_down">
          <img class="card-img-top" src="./images/menu3.jpg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Etiam porta sem malesuada magna mollis euismod.</p>
            <p class="card-text"><small>23,500 B</small></p>
            <button>ADD TO CART</button>
          </div>
        </div>
      </div>
    </div>


    

  </div>

</div>


</div>



<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a4376edef3289a6"></script>


    </div> <!-- /container -->
  </main>

<?php include "footer.php";?>



<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a4376edef3289a6"></script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


<script>
  jQuery(document).ready(function(){
    $('#spinner .input-group-addon').on('click', function(){
        let input = $(this).closest('#spinner').find('input[name=qty]');

        if($(this).data('action') === 'increment') {
            if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                input.val(parseInt(input.val(), 10) + 1);
            }
        } else if($(this).data('action') === 'decrement') {
            if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                input.val(parseInt(input.val(), 10) - 1);
            }
        }
    });
});

  jQuery(document).ready(function(){
    $('#spinner2 .input-group-addon').on('click', function(){
        let input = $(this).closest('#spinner2').find('input[name=qty]');

        if($(this).data('action') === 'increment') {
            if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                input.val(parseInt(input.val(), 10) + 1);
            }
        } else if($(this).data('action') === 'decrement') {
            if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                input.val(parseInt(input.val(), 10) - 1);
            }
        }
    });
});

</script>

</body>
</html>