<!doctype html>
<html lang="en">
<head>
  <title>Hello, world!</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./css/app.css">
</head>
<body>
  <?php include "head.php";?>
  <main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-image: url('../images/cover-01.jpg')">
      <h1 class="align-middle">เช่า/ซื้อ <strong>GADGET</strong> สำหรับคุณ</h1>
      <!-- <div class="container"> -->
      <!-- <img src="./images/cover-01.jpg"> -->
      <!-- <a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p> -->
      <!-- </div> -->
    </div>

    <div class="container">

      <div id="carouselA" class="carousel slide mt-5" data-ride="false">
  <!-- <ol class="carousel-indicators">
    <li data-target="#carouselAIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselAIndicators" data-slide-to="1"></li>
    <li data-target="#carouselAIndicators" data-slide-to="2"></li>
  </ol> -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <!-- large shows 5, small shows 2, x-small shows 1 -->
      <div class="row">
        <div class="col-sm"><img src="./images/menu1.jpg" alt="First slide"></div>
        <div class="col-sm hid_xs_down"><img src="./images/menu2.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu3.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu4.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu5.jpg" alt="First slide"></div>
      </div>
    </div>
    <div class="carousel-item">
      <!-- large shows 5, small shows 2, x-small shows 1 -->
      <div class="row">
        <div class="col-sm"><img src="./images/menu1.jpg" alt="First slide"></div>
        <div class="col-sm hid_xs_down"><img src="./images/menu2.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu3.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu4.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu5.jpg" alt="First slide"></div>
      </div>
    </div>
    <div class="carousel-item">
      <!-- large shows 5, small shows 2, x-small shows 1 -->
      <div class="row">
        <div class="col-sm"><img src="./images/menu1.jpg" alt="First slide"></div>
        <div class="col-sm hid_xs_down"><img src="./images/menu2.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu3.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu4.jpg" alt="First slide"></div>
        <div class="col-sm hid_md_down"><img src="./images/menu5.jpg" alt="First slide"></div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselA" role="button" data-slide="prev">
    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
    <i class="fas fa-chevron-left"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselA" role="button" data-slide="next">
    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
    <i class="fas fa-chevron-right"></i>
    <span class="sr-only">Next</span>
  </a>
</div>



<div id="carouselB" class="carousel slide mt-3" data-ride="false">
  <!-- <ol class="carousel-indicators">
    <li data-target="#carouselBIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselBIndicators" data-slide-to="1"></li>
    <li data-target="#carouselBIndicators" data-slide-to="2"></li>
  </ol> -->
  <div class="overlay_top">
    <!-- <img src="./images/TriangleW.png"> -->
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active" style="background-image: url('images/hero/heroes_1.jpg');">
      <div class="intro_left"><h3>Heading</h3>Donec id elit non mi porta gravida at eget metus. View details »</div>
    </div>
    <div class="carousel-item" style=" background-image: url('images/hero/heroes_2.jpg');">
      <div class="intro_center" style="width: 100%"><h3>Heading</h3>Donec id elit non mi porta gravida at eget metus. View details »</div>
    </div>
    <div class="carousel-item" style="  background-image: url('images/hero/heroes_3.jpg');">
      <div class="intro_right"><h3>Heading</h3>Donec id elit non mi porta gravida at eget metus. View details »</div>
    </div>
  </div>
  <div class="overlay_bottom"></div>

  <a class="carousel-control-prev" href="#carouselB" role="button" data-slide="prev">
    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
    <i class="fas fa-chevron-left"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselB" role="button" data-slide="next">
    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
    <i class="fas fa-chevron-right"></i>
    <span class="sr-only">Next</span>
  </a>
</div>



<div>
<ul class="nav nav-pills mb-3 mt-5 justify-content-center" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-new-tab" data-toggle="pill" href="#pills-new" role="tab" aria-controls="pills-new" aria-selected="true">New Arrivals</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-top-tab" data-toggle="pill" href="#pills-top" role="tab" aria-controls="pills-top" aria-selected="false">Top Rated </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-sale-tab" data-toggle="pill" href="#pills-sale" role="tab" aria-controls="pills-sale" aria-selected="false">Onsale</a>
  </li>
</ul>

<div class="tab-content mb-5" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-new" role="tabpanel" aria-labelledby="pills-new-tab">
    <?php include "thumbnails.php";?>
  </div>
  <div class="tab-pane fade" id="pills-top" role="tabpanel" aria-labelledby="pills-top-tab">
    <?php include "thumbnails.php";?>
  </div>
  <div class="tab-pane fade" id="pills-sale" role="tabpanel" aria-labelledby="pills-sale-tab">
    <?php include "thumbnails.php";?>
  </div>
</div>
</div>

<!-- Example row of columns -->
<!-- <div class="row mt-5">
  <div class="col-md-4">

    <h2>Heading <i class="fas fa-user"></i></h2>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
  </div>
  <div class="col-md-4">
    <h2>Heading <i class="fab fa-facebook"></i></h2>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
  </div>
  <div class="col-md-4">
    <h2>Heading <i class="fas fa-cogs"></i></h2>
    <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
    <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
  </div>
</div> -->

<!-- <div class="row"> -->
<div class="triangle_line"> </div>
<!-- </div> -->
<!-- <hr> -->

</div> <!-- /container -->

</main>

<?php include "footer.php";?>





<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>