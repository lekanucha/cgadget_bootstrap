<footer>
	<div class="container">
	<div class="row">
		<div class="col">
			<div>
				<p class="mb-0">Need support? <strong>Call 0xx xxx-xxxx</strong></p>
				<div class="my-2">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
					<a href="#"><i class="fab fa-skype"></i></a>
					<a href="#"><i class="fab fa-google-plus-g"></i></a>
				</div>
				<p>&copy; 2018 ceegadget</p>
			</div>
		</div>
		<div class="col bg_2">
			<div>
				<h5>Subscription</h5>
				<p>To receive latest offers and discount form ceegadget</p>
				<form class="form">
					<input type="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
				</form>
			</div>
		</div>
		<div class="col">
			<div>
				<h5>Payment Methods</h5>
				<p>We support one of the following payment methods</p>
				<div class="my-2">
					<a href="#"><i class="fab fa-cc-paypal"></i></a>
					<a href="#"><i class="fab fa-cc-visa"></i></a>
					<a href="#"><i class="fab fa-cc-mastercard"></i></a>
					<a href="#"><i class="fab fa-bitcoin"></i></a>
				</div>
			</div>
		</div>
		<div class="overlay_bottom"> </div>
	</div>
	</div>
</footer>